#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QLineEdit>

/// \file

/// \fn MainWindow::MainWindow(QWidget *parent)
/// \~English
/// \brief Constructor
/// \~Spanish
/// \brief Constructor
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

/// \fn MainWindow::~MainWindow()
/// \~English
/// \brief Destructor
/// \~Spanish
/// \brief Destructor
MainWindow::~MainWindow()
{
    delete ui;
}

/// \fn void MainWindow::on_lineEdit_textChanged(const QString &arg1)
/// \~English
/// \brief Function that is called every time the password text box field is
/// changed.
/// \param arg1 string with the current value of the password text box
/// \~Spanish
/// \brief Funcion que es llamada cada ves que la caja de texto para la contrasena
/// es cambiada.
/// \param arg1 cadena con el valor corriente de la caja de texto para la contrasena.
void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    setNumberOfCharacters(0,0);
    setUpperCharacters(0,0);
    setLowerCharacters(0,0);
    setDigits(0,0);
    setDigitsOnly(0,0);
    setSymbols(0,0);
    setConsecutiveDigits(0,0);
    setConsecutiveLower(0,0);
    setConsecutiveUpper(0,0);
    setMiddleDigitsOrSymbols(0,0);
    setRequirements(0,0);
    setLettersOnly(0,0);
    // The function the student needs to work with
    // La funcion en la que el estudiante va a trabajar.
    readPass(arg1.toStdString());
}

/// \fn void MainWindow::on_HiddenCheckBox_clicked(bool checked)
/// \~English
/// \brief Function that is called when the password hidden checkbox is selected.
/// \param checked boolean value
/// \~Spanish
/// \brief Funcion que es llamada cuando la caja de seleccion (checkbox) de esconder
/// el password es seleccionada.
void MainWindow::on_HiddenCheckBox_clicked(bool checked)
{
    if (checked) ui->lineEdit->setEchoMode(QLineEdit::Password);
    else ui->lineEdit->setEchoMode(QLineEdit::Normal);
}

/// \fn void MainWindow::setNumberOfCharacters(int count, int score)
/// \~English
/// \brief Function to set the number of characters and score in the GUI
/// \param count the number of characters found.
/// \param score the score given by the count of characters found.
/// \~Spanish
/// \brief Funcion para establecer el number de caracteres y la puntuacion en el GUI
/// \param count el numero de letras encontrado.
/// \param score le puntuacion dada por la cuenta de las letras.
void MainWindow::setNumberOfCharacters(int count, int score){
    ui->NumberOfCharCount->setText(QString::number(count));
    ui->NumberOfCharScore->setText(QString::number(score));
    ui->NumberCharCheckBox->setChecked(count > 0);


}

/// \fn void MainWindow::setUpperCharacters(int count, int score)
/// \~English
/// \brief Function to set the number of upper case characters and score in the GUI
/// \param count the number of upper case characters  found.
/// \param score the score given by the count of upper case characters.
/// \~Spanish
/// \brief Funcion para establecer el number de caracteres mayusculas y la puntuacion en el GUI
/// \param count el numero de letras mayusculas encontrado.
/// \param score le puntuacion dada por la cuenta de letras mayusculas encontrada.
void MainWindow::setUpperCharacters(int count, int score){
    ui->UppercaseCountLabel->setText(QString::number(count));
    ui->UppercaseScoreLabel->setText(QString::number(score));
    ui->UppercaseCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setLowerCharacters(int count, int score)
/// \~English
/// \brief Function to set the number of lower case characters and score in the GUI
/// \param count the number of lower case characters  found.
/// \param score the score given by the count of lower case characters.
/// \~Spanish
/// \brief Funcion para establecer el number de caracteres minusculas y la puntuacion en el GUI
/// \param count el numero de letras minusculas encontrado.
/// \param score le puntuacion dada por la cuenta de letras minusculas encontrada.
void MainWindow::setLowerCharacters(int count, int score){
    ui->LowerCaseCountLabel->setText(QString::number(count));
    ui->LowerCaseScoreLabel->setText(QString::number(score));
    ui->LowercaseCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setDigits(int count, int score)
/// \~English
/// \brief Function to set the number of digits characters and score in the GUI
/// \param count the number of digits  found.
/// \param score the score given by the count of digit characters.
/// \~Spanish
/// \brief Funcion para establecer el number de digitos y la puntuacion en el GUI
/// \param count el numero de digitos encontrado.
/// \param score le puntuacion dada por la cuenta de digitos encontrada.
void MainWindow::setDigits(int count, int score){
    ui->NumbersCountlabel->setText(QString::number(count));
    ui->NumberScorelabel->setText(QString::number(score));
    ui->NumbersCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setSymbols(int count, int score)
/// \~English
/// \brief Function to set the number of symbols characters and score in the GUI
/// \param count the number of symbols  found.
/// \param score the score given by the count of symbols.
/// \~Spanish
/// \brief Funcion para establecer el number de simbolos y la puntuacion en el GUI
/// \param count el numero de simbolos encontrado.
/// \param score le puntuacion dada por la cuenta de simbolos encontrada.
void MainWindow::setSymbols(int count, int score){
    ui->SymbolsCountlabel->setText(QString::number(count));
    ui->SymbolScorelabel->setText(QString::number(score));
    ui->SymbolsCheckBox->setChecked(count > 0);

}

/// \fn void MainWindow::setMiddleDigitsOrSymbols(int count, int score)
/// \~English
/// \brief Function to set the number of middle digits or symbols and score in the GUI
/// \param count the number of middle digits or symbols  found.
/// \param score the score given by the count of middle digits or symbols.
/// \~Spanish
/// \brief Funcion para establecer el number de simbolos o digitos entre medio y la puntuacion en el GUI
/// \param count el numero de simbolos y digitos entre medio encontrado.
/// \param score le puntuacion dada por la cuenta de simbolos y digitos entre medio encontrada.
void MainWindow::setMiddleDigitsOrSymbols(int count, int score){
    ui->MidNumOrSymCountlabel->setText(QString::number(count));
    ui->MidNumOrSymScorelabel->setText(QString::number(score));
    ui->MiddleCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setRequirements(int count, int score)
/// \~English
/// \brief Function to set the number password strength requirements met and score in the GUI
/// \param count the number of requirements met.
/// \param score the score given by requirements.
/// \~Spanish
/// \brief Funcion para establecer el number de requisitos cumplidos para password fuertes
///  y la puntuacion en el GUI
/// \param count el numero de requisitos .
/// \param score le puntuacion dada por la cuenta requisitos.
void MainWindow::setRequirements(int count, int score){
    ui->ReqCountlabel->setText(QString::number(count));
    ui->ReqScorelabel->setText(QString::number(score));
    ui->RequirementCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setLettersOnly(int count, int score)
/// \~English
/// \brief Function to set the number of letters if there were only letters and score in the GUI
/// \param count the number of letters only found.
/// \param score the score given by letters only found.
/// \~Spanish
/// \brief Funcion para establecer el number de letras si solo habían letras
///  y la puntuacion en el GUI
/// \param count el numero de letras solamente encontradas .
/// \param score le puntuacion dada por la cuenta solamente de letras encontradas.
void MainWindow::setLettersOnly(int count, int score){
    ui->LettersOnlyCountLabel->setText(QString::number(count));
    ui->LettersOnlyScoreLabel->setText(QString::number(score));
    ui->LettersOnlyCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setDigitsOnly(int count, int score)
/// \~English
/// \brief Function to set the number of digits if there were only digits and score in the GUI
/// \param count the number of digits only found.
/// \param score the score given by digits only found.
/// \~Spanish
/// \brief Funcion para establecer el number de digitos si solo habían digitos
///  y la puntuacion en el GUI
/// \param count el numero de digitos solamente encontradas .
/// \param score le puntuacion dada por la cuenta solamente de digitos encontradas.
void MainWindow::setDigitsOnly(int count, int score){
    ui->NumOnlyCountLabel->setText(QString::number(count));
    ui->NumOnlyScoreLabel->setText(QString::number(score));
    ui->NumbersOnlyCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setConsecutiveUpper(int count, int score)
/// \~English
/// \brief Function to set the number of consecutive upper characters and score in the GUI
/// \param count the number of consecutive upper characters found.
/// \param score the score given by the count of consecutive upper characters found.
/// \~Spanish
/// \brief Funcion para establecer el number de letras mayusculas consecutivas y la puntuacion en el GUI
/// \param count el numero de letras mayusculas consecutivas encontrado.
/// \param score le puntuacion dada por la cuenta de las letras mayusculas consecutivas.
void MainWindow::setConsecutiveUpper(int count, int score){
    ui->ConsUpperCountLabel->setText(QString::number(count));
    ui->ConsUpperScoreLabel->setText(QString::number(score));
    ui->ConsecutiveUpperCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setConsecutiveLower(int count, int score)
/// \~English
/// \brief Function to set the number of consecutive lower characters and score in the GUI
/// \param count the number of consecutive lower characters found.
/// \param score the score given by the count of consecutive lower characters found.
/// \~Spanish
/// \brief Funcion para establecer el number de letras minusculas consecutivas y la puntuacion en el GUI
/// \param count el numero de letras minusculas consecutivas encontrado.
/// \param score le puntuacion dada por la cuenta de las letras minusculas consecutivas.
void MainWindow::setConsecutiveLower(int count, int score){
    ui->ConsLowerCountLabel->setText(QString::number(count));
    ui->ConsLowerScoreLabel->setText(QString::number(score));
    ui->ConsecutiveLowerCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::setConsecutiveDigits(int count, int score)
/// \~English
/// \brief Function to set the number of consecutive digits and score in the GUI
/// \param count the number of consecutive digits found.
/// \param score the score given by the count of consecutive digits found.
/// \~Spanish
/// \brief Funcion para establecer el number de digitos consecutivos y la puntuacion en el GUI
/// \param count el numero de digitos consecutivos encontrado.
/// \param score le puntuacion dada por la cuenta de los digitos consecutivos.
void MainWindow::setConsecutiveDigits(int count, int score){
    ui->ConsNumsCountLabel->setText(QString::number(count));
    ui->ConsNumsScoreLabel->setText(QString::number(score));
    ui->ConsecutiveNumbersCheckBox->setChecked(count > 0);
}

/// \fn void MainWindow::strengthDisplay(string strength, int totalScore)
/// \~English
/// \brief Function to set the computed strength and total score password
/// strength in the GUI.
/// \param strength Computed strenght of the password.
/// \param totalScore Total score of the strength of the password.
/// \~Spanish
/// \brief Funcion para establecer la fuerza y la puntuacion total calculada de la contrasena.
/// \param strength Fuerza computada de la contrasena.
/// \param totalScore Puntuacion total de la fuerza de la contrasena.
void MainWindow::strengthDisplay(string strength, int totalScore){

    if(totalScore > 100){
        totalScore = 100;
        QString percent = QString::number(totalScore) + "%";
        ui->progressBar->setValue(totalScore);
        ui->progressLabel->setText(percent);
    }
    else if(totalScore < 0){
        totalScore = 0;
        QString percent = QString::number(totalScore) + "%";
        ui->progressBar->setValue(totalScore);
        ui->progressLabel->setText(percent);
    }
    else{
        QString percent = QString::number(totalScore) + "%";
        ui->progressBar->setValue(totalScore);
        ui->progressLabel->setText(percent);
    }


    ui->statusLabel->setText(QString::fromStdString(strength));
}




